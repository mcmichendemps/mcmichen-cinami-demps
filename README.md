McMichen, Cinami & Demps is one of the top divorce law firms in Orlando, FL. With over 50 years of combined experience, the family law attorneys at McMichen, Cinami & Demps have helped countless families in all kinds of cases. When it comes to your family, seek proven experience with decades of success. 

Our team includes a Board Certified specialist in Marital and Family Law as well as a Florida Supreme Court Certified Family Mediator. With these certifications and our decades of experience, we are sure to be a great choice for your family law matters. 

The attorneys at McMichen, Cinami & Demps have a great track record with all kinds of family law cases, including: child support, child custody, spousal support, property division, modifications of order, and more. They also handle all kinds of divorce, including high-asset divorce, LGBT divorce, and more. 

Contact us today for a free consultation. When it comes to protecting your family, there is no time to waste. That is why we always offer a free, no-obligation consultation. We will discuss your case with you and more forward only when you are ready to. 

McMichen, Cinami & Demps

1500 E Concord St
Orlando, FL 32803

(407) 898-2161

https://fldivorce.com